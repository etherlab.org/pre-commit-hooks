# Pre-Commit Hooks

Pre-Commit hooks for Git used in the EtherLab project.

```yaml
-   repo: https://gitlab.com/etherlab.org/pre-commit-hooks.git
    rev: '' # insert revision here
    hooks:
    -   id: clang-format-10
```

# Hooks

## update-git-latex

Store Git version information in LaTeX-includable file git.tex. Sets the date
(`\today`) to the commit date and defines the below variables:

   - `\revision` (Short commit hash)
   - `\gitversion` (Version string, see git describe))
   - `\gitauthor` (Author name)

## clang-format-10

Execute clang-format version 10.
