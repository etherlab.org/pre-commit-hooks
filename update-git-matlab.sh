#!/bin/sh
#
# $Id: update-hg.sh,v 708428cd840a 2016/05/21 20:31:11 fp $
#
# Output information about parent revision to TeX-includable file.
#

tmpl="git_revision='%h'; git_author='%an';"	


git show -s --format="$tmpl" HEAD > git.m

echo "Updated git.m."
