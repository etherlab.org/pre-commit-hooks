#!/bin/sh
#
# $Id: update-hg.sh,v 708428cd840a 2016/05/21 20:31:11 fp $
#
# Output information about parent revision to TeX-includable file.
#

tmpl='\def\revision{%h}
\def\gitversion{%(describe)}
\def\gitauthor{%an}
\def\isodate#1-#2-#3x{
  \day  = #3
  \month= #2
  \year = #1
}
\isodate %csx
'

git show -s --format="${tmpl}" HEAD > git.tex

echo "Updated git.tex."
